package org.ksga.kshrdacademy.configuration;

import lombok.Data;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Configuration
@EnableSwagger2
public class SwaggerConfiguration {
    @Bean
	public Docket api() {
        		return new Docket(DocumentationType.SWAGGER_2)
        			.select()
        			.apis(RequestHandlerSelectors.basePackage("org.ksga.kshrdacademy.controller"))
        			.paths(PathSelectors.any())
        			.build()
        			.directModelSubstitute(LocalDate.class, java.sql.Date.class)
        			.directModelSubstitute(LocalDateTime.class, java.util.Date.class)
        			.apiInfo(apiInfo());
        	}

    private ApiInfo apiInfo() {
        		return new ApiInfoBuilder()
        			.title("KSHRD Academy APIs")
        			.description("KSHRD Academy APIs description")
        			.version("1.0.0")
        			.contact(new Contact(null, null, null))
        			.build();
        	}
}
