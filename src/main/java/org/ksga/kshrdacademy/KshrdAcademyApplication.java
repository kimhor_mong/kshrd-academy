package org.ksga.kshrdacademy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KshrdAcademyApplication {

    public static void main(String[] args) {
        SpringApplication.run(KshrdAcademyApplication.class, args);
    }

}
